package woniusinffer;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

public class UIControler implements ActionListener{
	   private CaptureUI ui;
	   private static ArpPacketCapture arpPacketCapture=new ArpPacketCapture();
	   private static Thread arpThread=new Thread(arpPacketCapture);
	   public UIControler(CaptureUI ui){
		   this.ui=ui;
	   }
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource().equals(ui.startButton)){
			ui.setCanStopStatus();
			String deviceName=ui.deviceList.getSelectedItem().toString();
			boolean is_hunza=ui.hzcheckbox.isSelected();
			arpPacketCapture.setDeviceName(deviceName);
			arpPacketCapture.setUi(ui);
			arpPacketCapture.setHz(is_hunza);
			arpPacketCapture.setCanService(true);
			//开启抓包线程
			if(!arpThread.isAlive()){arpThread.start();}
			@SuppressWarnings("deprecation")
			String info="\n***************************抓包开始("+new Date().toLocaleString()+")***************************";
			ui.addMessage(info);
			arpPacketCapture.log(info);
			

		}else if (e.getSource().equals(ui.stopButton)) {
			ui.setCanStarStatus();
			arpPacketCapture.setCanService(false);
			@SuppressWarnings("deprecation")
			String info="\n***************************抓包停止("+new Date().toLocaleString()+")***************************";
			ui.addMessage(info);
			arpPacketCapture.log(info);
		}else if (e.getSource().equals(ui.clearButton)) {
			ui.mainTextArea.setText("");
		}
		
	}
}
